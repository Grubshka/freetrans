#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QKeySequence>
#include <QSettings>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::SettingsDialog)
{
    m_ui->setupUi(this);

    QSettings settings;
    settings.beginGroup("player");
    m_ui->volumeDownKeyEdit->setKeySequence(QKeySequence(settings.value("volumeDown/shortcut", "F10").toString()));
    m_ui->volumeDownStepsEdit->setValue(settings.value("volumeDown/steps", 1).toInt());

    m_ui->volumeUpKeyEdit->setKeySequence(QKeySequence(settings.value("volumeUp/shortcut", "F11").toString()));
    m_ui->volumeUpStepsEdit->setValue(settings.value("volumeUp/steps", 1).toInt());

    m_ui->rewindKeyEdit->setKeySequence(QKeySequence(settings.value("rewind/shortcut", "").toString()));
    m_ui->rewindDurationEdit->setValue(settings.value("rewind/duration", 3000).toInt());

    m_ui->shortRewindKeyEdit->setKeySequence(QKeySequence(settings.value("shortRewind/shortcut", "F6").toString()));
    m_ui->shortRewindDurationEdit->setValue(settings.value("shortRewind/duration", 3000).toInt());

    m_ui->forwardKeyEdit->setKeySequence(QKeySequence(settings.value("forward/shortcut", "").toString()));
    m_ui->forwardDurationEdit->setValue(settings.value("forward/duration", 3000).toInt());

    m_ui->shortForwardKeyEdit->setKeySequence(QKeySequence(settings.value("shortForward/shortcut", "F8").toString()));
    m_ui->shortForwardDurationEdit->setValue(settings.value("shortForward/duration", 3000).toInt());

    m_ui->playPauseKeyEdit->setKeySequence(QKeySequence(settings.value("playPause/shortcut", "F7").toString()));
    m_ui->playRewindDurationEdit->setValue(settings.value("playPause/rewindDuration", 3000).toInt());
    settings.endGroup();
}

SettingsDialog::~SettingsDialog()
{
    delete m_ui;
}

void SettingsDialog::on_SettingsDialog_accepted()
{
    QSettings settings;
    settings.beginGroup("player");
    settings.setValue("volumeDown/shortcut", m_ui->volumeDownKeyEdit->keySequence().toString());
    settings.setValue("volumeDown/steps", m_ui->volumeDownStepsEdit->value());

    settings.setValue("volumeUp/shortcut", m_ui->volumeUpKeyEdit->keySequence().toString());
    settings.setValue("volumeUp/steps", m_ui->volumeUpStepsEdit->value());

    settings.setValue("rewind/shortcut", m_ui->rewindKeyEdit->keySequence().toString());
    settings.setValue("rewind/duration", m_ui->rewindDurationEdit->value());

    settings.setValue("shortRewind/shortcut", m_ui->shortRewindKeyEdit->keySequence().toString());
    settings.setValue("shortRewind/duration", m_ui->shortRewindDurationEdit->value());

    settings.setValue("forward/shortcut", m_ui->forwardKeyEdit->keySequence().toString());
    settings.setValue("forward/duration", m_ui->forwardDurationEdit->value());

    settings.setValue("shortForward/shortcut", m_ui->shortForwardKeyEdit->keySequence().toString());
    settings.setValue("shortForward/duration", m_ui->shortForwardDurationEdit->value());

    settings.setValue("playPause/shortcut", m_ui->playPauseKeyEdit->keySequence().toString());
    settings.setValue("playPause/rewindDuration", m_ui->playRewindDurationEdit->value());
    settings.endGroup();
}
