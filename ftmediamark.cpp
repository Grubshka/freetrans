#include "ftmediamark.h"

FtMediaMark::FtMediaMark()
{

}

FtMediaMark::FtMediaMark(const qint64 &in_time) :
  m_time(in_time)
{

}

FtMediaMark::FtMediaMark(const qint64 &in_time, const QString &in_title) :
  m_time(in_time),
  m_title(in_title)
{

}

qint64 FtMediaMark::time() const
{
  return m_time;
}

void FtMediaMark::setTime(const qint64 &value)
{
  m_time = value;
}

QString FtMediaMark::title() const
{
  return m_title;
}

void FtMediaMark::setTitle(const QString &value)
{
  m_title = value;
}

void FtMediaMark::read(const QJsonObject &json)
{
  m_title = json["title"].toString();
  m_time = (qint64) json["title"].toDouble();
}

void FtMediaMark::write(QJsonObject &json) const
{
    json["title"] = m_title;
    json["time"] = m_time;
}
