#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMetaObject>
#include <QCloseEvent>
#include <QListWidgetItem>
#include <QDir>
#include <QVector>
#include "qxtglobalshortcut.h"
#include "ftplayer.h"
#include "ftmedialist.h"
#include "ftmedialistmodel.h"
#include "ftmedialistproviderdir.h"

namespace Ui {
    class FtWindow;
}

class FtWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FtWindow(QWidget * in_parent = 0);
    ~FtWindow();

    void closeEvent(QCloseEvent* event);
private slots:
    void on_actionQuit_triggered();

    void on_actionAdd_triggered();

    void on_actionRemove_triggered();

    void onPlayerPositionChanged(qint64 in_position);
    void onPlayerDurationChanged(qint64 in_duration);
    void onPlayerVolumeChanged(int in_volume);
    void onPlayerStateChanged(FtPlayer::State in_state);
    void onPlayerPlaybackRateChanged(qreal in_rate);
    // void onPlayerMediaStatusChanged(QMediaPlayer::MediaStatus status);

    void onMediaList_currentChanged();

    void on_timeSlider_sliderPressed();
    void on_timeSlider_sliderReleased();
    void on_timeSlider_sliderMoved(int in_position);

    void on_volumeSlider_sliderMoved(int in_position);

    void on_mediaList_doubleClicked(const QModelIndex &index);
    void onMediaListSelectionModel_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void onMediaListModel_dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = QVector<int>());

    void on_playbackRateSlider_sliderMoved(int position);

    void on_actionPlayPause_triggered(bool in_checked);
    void on_actionShortRewind_triggered();
    void on_actionRewind_triggered();
    void on_actionShortForward_triggered();
    void on_actionForward_triggered();
    void on_actionVolumeDown_triggered();
    void on_actionVolumeUp_triggered();

    void on_actionAbout_triggered();

    void on_actionSelectAll_triggered();

    void on_actionSettings_triggered();

private:
    Ui::FtWindow* m_ui;
    FtPlayer m_player;

    QVector<FtMediaListProviderVirtual*> m_mediaListProviders;
    FtMediaListProviderDir m_localProvider;

    FtMediaListStorable* m_currentMediaList;
    FtPlayer::State m_savedState;

    FtMediaListModel *m_mediaListModel;
    QItemSelectionModel *m_mediaListSelectionModel;

    QVector<QxtGlobalShortcut*> m_shortcuts;
    void setShortcuts();
    void unsetShortcuts();

    void about();
    void updateMediaListUI();
    void updatePlayerUI();
    QDir getDefaultProviderDir() const;
};

#endif // MAINWINDOW_H
