#ifndef FTMEDIALISTFILE_H
#define FTMEDIALISTFILE_H

#include <ftmedialist.h>
#include <QFile>
#include <QJsonObject>

class FtMediaListFile : public FtMediaListStorable
{
public:
    FtMediaListFile();
    FtMediaListFile(QFile *file);

    bool read();
    bool write();
protected:
    QFile* m_file;

    void fromJson(const QJsonObject &in_json);
    QJsonObject toJson() const;
};

#endif // FTMEDIALISTFILE_H
