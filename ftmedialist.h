#ifndef FTMEDIALIST_H
#define FTMEDIALIST_H

#include <QJsonObject>
#include <QUrl>
#include <QString>
#include <QObject>
#include "ftmedia.h"

class FtMediaList: public QObject
{
  Q_OBJECT
public:
  FtMediaList();

  std::vector<FtMedia>::iterator find(const FtMedia& in_media) const;
  std::vector<FtMedia>::iterator find(const QUrl& in_url) const;

  bool contains(const FtMedia& in_media) const;
  bool contains(const QUrl& in_url) const;

  FtMedia& at(const int& in_index);
  int count();

  void append(const FtMedia& in_media);
  void insert(const FtMedia& in_media, const int& in_mediaIndex);
  void remove(const int& in_mediaIndex);
  void remove(const int& in_mediaIndex, const int& in_count);
  void remove(const FtMedia& in_media);

  bool hasCurrent() const;
  std::vector<FtMedia>::iterator current() const;
  void setCurrent(const FtMedia& in_media);
  void setCurrent(const int& in_mediaIndex);
  void setCurrent(const std::vector<FtMedia>::iterator& in_mediaIterator);
  void setCurrent();


  QString title() const;
  void setTitle(const QString &in_title);

signals:
  void currentChanged();
  void mediaInserted(int in_mediaIndex);
  void mediaRemoved(int in_mediaIndex);
  
protected:
  std::vector<FtMedia>::iterator m_current;
  mutable std::vector<FtMedia> m_medias;
  QString m_title;
};

class FtMediaListStorable: public FtMediaList
{
public:
    virtual bool read() = 0;
    virtual bool write() = 0;
};


#endif // FTMEDIALIST_H:w
