#ifndef FTMEDIAMARK_H
#define FTMEDIAMARK_H

#include <QtGlobal>
#include <QString>
#include <QJsonObject>

/**
 * @brief The FtMediaMark handles a bookmark in a media
 */
class FtMediaMark {
public:
  FtMediaMark();
  FtMediaMark(const qint64 &in_time);
  FtMediaMark(const qint64 &in_time, const QString &in_title);

  qint64 time() const;
  void setTime(const qint64 &value);

  QString title() const;
  void setTitle(const QString &value);

  void read(const QJsonObject &json);
  void write(QJsonObject &json) const;

private:
  qint64 m_time;
  QString m_title;
};

#endif // FTMEDIAMARK_H
