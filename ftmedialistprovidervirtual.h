#ifndef FTMEDIALISTPROVIDERVIRTUAL_H
#define FTMEDIALISTPROVIDERVIRTUAL_H

#include <QStringList>
#include <ftmedialist.h>

class FtMediaListProviderVirtual
{
public:
    virtual QVector<FtMediaListStorable*> getAll() const = 0;
    virtual FtMediaListStorable* get(const QString & listName) const = 0;
};

#endif // FTMEDIALISTPROVIDERVIRTUAL_H
