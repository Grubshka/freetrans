#include "kitimeslider.h"

#include <QDebug>
#include <QStyle>
#include <QStyleOptionSlider>
#include <QRect>
#include <QMouseEvent>
#include <QTime>
#include <QToolTip>

/*KiTimeSlider::KiTimeSlider()
    : KiSlider()
{

}*/

void KiTimeSlider::mouseMoveEvent(QMouseEvent *event)
{
    QStyleOptionSlider opt;
    initStyleOption(&opt);
    QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);

    int newVal;
    if (orientation() == Qt::Vertical) {
      double halfHandleHeight = (0.5 * sr.height()) + 0.5; // Correct rounding
      int adaptedPosY = event->y();
      if (adaptedPosY < halfHandleHeight) adaptedPosY = halfHandleHeight;
      if (adaptedPosY > height() - halfHandleHeight) adaptedPosY = height() - halfHandleHeight;
      // get new dimensions accounting for slider handle width
      double newHeight = (height() - halfHandleHeight) - halfHandleHeight;
      double normalizedPosition = (adaptedPosY - halfHandleHeight)  / newHeight ;

      newVal = minimum() + ((maximum()-minimum()) * normalizedPosition);
    }
    else {
      double halfHandleWidth = (0.5 * sr.width()) + 0.5; // Correct rounding
      int adaptedPosX = event->x();
      if (adaptedPosX < halfHandleWidth) adaptedPosX = halfHandleWidth;
      if (adaptedPosX > width() - halfHandleWidth) adaptedPosX = width() - halfHandleWidth;
      // get new dimensions accounting for slider handle width
      double newWidth = (width() - halfHandleWidth) - halfHandleWidth;
      double normalizedPosition = (adaptedPosX - halfHandleWidth)  / newWidth ;

      newVal = minimum() + ((maximum()-minimum()) * normalizedPosition);
    }

    QTime time(0,0,0,0);
    time = time.addMSecs(newVal);
    // setToolTip(time.toString());
    QToolTip::showText(mapToGlobal(QPoint(event->x(), event->y())), time.toString());

    event->accept();
    KiSlider::mouseMoveEvent(event);
}
