#ifndef FTMEDIALISTVIEW_H
#define FTMEDIALISTVIEW_H


#include <QWidget>
#include <QListView>
#include <QPainter>
#include <QAbstractItemDelegate>

#include "ftmedia.h"


class FtMediaListView: public QListView
{
public:
  FtMediaListView(QWidget* parent = Q_NULLPTR);
  virtual ~FtMediaListView();

};


class FtMediaListDelegate: public QAbstractItemDelegate
{
public:
  FtMediaListDelegate(QObject* parent = 0);
  virtual ~FtMediaListDelegate();

  enum DataRole {
      fileNameRole = Qt::UserRole + 1,
      urlRole = Qt::UserRole + 2,
      playStateRole = Qt::UserRole + 3
  };

  void paint (QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;
  QSize sizeHint (const QStyleOptionViewItem& option, const QModelIndex& index) const;
};

Q_DECLARE_METATYPE(FtMediaListDelegate::DataRole)

#endif // FTMEDIALISTVIEW_H
