#include "kidefaultlabel.h"
#include "kidefaultlabelplugin.h"

#include <QtPlugin>

KiDefaultLabelPlugin::KiDefaultLabelPlugin(QObject *parent)
    : QObject(parent)
    , initialized(false)
{
}

void KiDefaultLabelPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (initialized)
        return;

    initialized = true;
}

bool KiDefaultLabelPlugin::isInitialized() const
{
    return initialized;
}

QWidget *KiDefaultLabelPlugin::createWidget(QWidget *parent)
{
    return new KiDefaultLabel(parent);
}

QString KiDefaultLabelPlugin::name() const
{
    return "KiDefaultLabel";
}

QString KiDefaultLabelPlugin::group() const
{
    return "Display Widgets";
}

QIcon KiDefaultLabelPlugin::icon() const
{
    return QIcon();
}

QString KiDefaultLabelPlugin::toolTip() const
{
    return "";
}

QString KiDefaultLabelPlugin::whatsThis() const
{
    return "";
}

bool KiDefaultLabelPlugin::isContainer() const
{
    return false;
}

QString KiDefaultLabelPlugin::domXml() const
{
    return "<ui language=\"c++\">\n"
           " <widget class=\"KiDefaultLabel\" name=\"kiDefaultLabel\">\n"
           "  <property name=\"toolTip\" >\n"
           "   <string>A label with default value</string>\n"
           "  </property>\n"
           "  <property name=\"whatsThis\" >\n"
           "   <string>A label with default value.</string>\n"
           "  </property>\n"
           " </widget>\n"
           "</ui>\n";
}

QString KiDefaultLabelPlugin::includeFile() const
{
    return "kidefaultlabel.h";
}
