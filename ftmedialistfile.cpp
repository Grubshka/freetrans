#include "ftmedialistfile.h"
#include <QJsonArray>
#include <QJsonDocument>

FtMediaListFile::FtMediaListFile(QFile* file)
    : m_file(file)
{

}

bool FtMediaListFile::read()
{
  if (m_file->exists()) {
    if (!m_file->open(QIODevice::ReadOnly)) {
      qWarning("Couldn't open save file.");
      return false;
    }

    QByteArray saveData = m_file->readAll();
    m_file->close();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    fromJson(loadDoc.object());

    return true;
  }
  return false;

}

bool FtMediaListFile::write()
{
    if (!m_file->open(QIODevice::WriteOnly)) {
      qWarning("Couldn't open save file.");
      return false;
    }

    QJsonDocument saveDoc(toJson());
    m_file->write(saveDoc.toJson());
    m_file->close();
    return true;
}

void FtMediaListFile::fromJson(const QJsonObject &in_json)
{
  m_title = in_json["title"].toString();

  QJsonArray mediaArray = in_json["medias"].toArray();
  for (int mediaIndex = 0; mediaIndex < mediaArray.size(); ++mediaIndex) {
      QJsonObject mediaObject = mediaArray[mediaIndex].toObject();
      FtMedia media;
      media.read(mediaObject);
      m_medias.push_back(media);

      if (QJsonValue::Undefined != mediaObject.value("current").type()) {
        setCurrent(m_medias.end()-1);
      }
  }
}

QJsonObject FtMediaListFile::toJson() const
{
  QJsonObject json;
  QJsonArray mediaArray;
  // foreach (const FtMedia& media, m_medias) {
  for (auto it = m_medias.begin(); it != m_medias.end(); ++it) {
    QJsonObject mediaObject;
    (*it).write(mediaObject);
    if (m_current == it) {
      mediaObject["current"] = true;
    }
    mediaArray.append(mediaObject);
  }
  json["medias"] = mediaArray;

  return json;
}
