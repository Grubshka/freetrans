#ifndef KIDEFAULTLABEL_H
#define KIDEFAULTLABEL_H

#include <QLabel>
#include <QObject>
#include <QString>

class Q_WIDGETS_EXPORT KiDefaultLabel : public QLabel
{
  Q_OBJECT
  Q_PROPERTY(QString defaultText READ defaultText WRITE setDefaultText RESET resetText)
public:
  explicit KiDefaultLabel(QWidget *parent=Q_NULLPTR, Qt::WindowFlags f=Qt::WindowFlags());
  explicit KiDefaultLabel(const QString &defaultText, QWidget *parent=Q_NULLPTR, Qt::WindowFlags f=Qt::WindowFlags());
  explicit KiDefaultLabel(const QString &defaultText, const QString &text, QWidget *parent=Q_NULLPTR, Qt::WindowFlags f=Qt::WindowFlags());

  QString defaultText() const;
  void setDefaultText(const QString &);
  bool isDefaultText() const;

public Q_SLOTS:
  void resetText();

private:
  QString m_defaultText;
};

#endif // KIDEFAULTLABEL_H
