#include "kislider.h"
#include <QMouseEvent>
#include <QStyleOptionSlider>
#include <QRect>

KiSlider::KiSlider(QWidget *parent)
  :QSlider(parent)
{
    setMouseTracking(true);
}

KiSlider::KiSlider(Qt::Orientation orientation, QWidget *parent)
  :QSlider(orientation, parent)
{
    setMouseTracking(true);
}

void KiSlider::mousePressEvent(QMouseEvent* event)
{
  QStyleOptionSlider opt;
  initStyleOption(&opt);
  QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);

  if (event->button() == Qt::LeftButton) {
    if (sr.contains(event->pos()) == false) {
        int newVal;
        if (orientation() == Qt::Vertical) {
            double halfHandleHeight = (0.5 * sr.height()) + 0.5; // Correct rounding
            int adaptedPosY = event->y();
            if (adaptedPosY < halfHandleHeight) adaptedPosY = halfHandleHeight;
            if (adaptedPosY > height() - halfHandleHeight) adaptedPosY = height() - halfHandleHeight;
            // get new dimensions accounting for slider handle width
            double newHeight = (height() - halfHandleHeight) - halfHandleHeight;
            double normalizedPosition = (adaptedPosY - halfHandleHeight)  / newHeight ;

            newVal = minimum() + ((maximum()-minimum()) * normalizedPosition);
        }
        else {
            double halfHandleWidth = (0.5 * sr.width()) + 0.5; // Correct rounding
            int adaptedPosX = event->x();
            if (adaptedPosX < halfHandleWidth) adaptedPosX = halfHandleWidth;
            if (adaptedPosX > width() - halfHandleWidth) adaptedPosX = width() - halfHandleWidth;
            // get new dimensions accounting for slider handle width
            double newWidth = (width() - halfHandleWidth) - halfHandleWidth;
            double normalizedPosition = (adaptedPosX - halfHandleWidth)  / newWidth ;

            newVal = minimum() + ((maximum()-minimum()) * normalizedPosition);
        }

        if (invertedAppearance() == true) setValue(maximum() - newVal);
        else setValue(newVal);
    }
    else {
        setCursor(Qt::ClosedHandCursor);
    }
    event->accept();
  }
  QSlider::mousePressEvent(event);
}

void KiSlider::mouseMoveEvent(QMouseEvent *event)
{
  QStyleOptionSlider opt;
  initStyleOption(&opt);
  if (event->buttons() & Qt::LeftButton) {
    setCursor(Qt::ClosedHandCursor);
  }
  else {
    QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
   if (sr.contains(event->pos())) {
      setCursor(Qt::OpenHandCursor);
    }
    else {
      setCursor(Qt::PointingHandCursor);
    }
  }
  event->accept();
  QSlider::mouseMoveEvent(event);
}

void KiSlider::mouseReleaseEvent(QMouseEvent *event)
{
    QStyleOptionSlider opt;
    initStyleOption(&opt);
    QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);

    if (sr.contains(event->pos())) {
      setCursor(Qt::OpenHandCursor);
    }
    else {
      setCursor(Qt::PointingHandCursor);
    }
    event->accept();
    QSlider::mouseReleaseEvent(event);
}
