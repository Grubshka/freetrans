#include "kidefaultlabel.h"

KiDefaultLabel::KiDefaultLabel(QWidget *parent, Qt::WindowFlags f)
    : QLabel(parent, f),
      m_defaultText("")
{
}

KiDefaultLabel::KiDefaultLabel(const QString &defaultText, QWidget *parent, Qt::WindowFlags f)
    : QLabel(defaultText, parent, f),
      m_defaultText(defaultText)
{

}

KiDefaultLabel::KiDefaultLabel(const QString &defaultText, const QString &text, QWidget *parent, Qt::WindowFlags f)
    : QLabel(text, parent, f),
      m_defaultText(defaultText)
{

}

QString KiDefaultLabel::defaultText() const
{
    return m_defaultText;
}

void KiDefaultLabel::setDefaultText(const QString & defaultText)
{
    m_defaultText = defaultText;
}

bool KiDefaultLabel::isDefaultText() const
{
    return defaultText().compare(text());
}

void KiDefaultLabel::resetText()
{
    QLabel::setText(defaultText());
}
