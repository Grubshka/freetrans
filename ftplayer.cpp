#include "ftplayer.h"

FtPlayer::FtPlayer() :
  FtPlayer(1000, 5000, 5000)
{

}

FtPlayer::FtPlayer(qint64 in_playRewindDuration, qint64 in_rewindDuration, qint64 in_forwardDuration) :
  m_playRewindDuration(in_playRewindDuration),
  m_rewindDuration(in_rewindDuration),
  m_forwardDuration(in_forwardDuration),
  m_loadingState(0),
  m_player()
{
  connect(&m_player, &QMediaPlayer::mediaStatusChanged,
          this, &FtPlayer::onPlayerMediaStatusChanged);
  connect(&m_player, &QMediaPlayer::stateChanged,
          this, &FtPlayer::onPlayerStateChanged);
  connect(&m_player, &QMediaPlayer::positionChanged,
          this, &FtPlayer::onPlayerPositionChanged);
  connect(&m_player, &QMediaPlayer::seekableChanged,
          this, &FtPlayer::onPlayerSeekableChanged);


  connect(&m_player, &QMediaPlayer::durationChanged,
          this, &FtPlayer::durationChanged);
  connect(&m_player, &QMediaPlayer::volumeChanged,
          this, &FtPlayer::volumeChanged);
  connect(&m_player, &QMediaPlayer::playbackRateChanged,
          this, &FtPlayer::playbackRateChanged);
}

qint64 FtPlayer::forwardDuration() const
{
  return m_forwardDuration;
}

void FtPlayer::setForwardDuration(qint64 value)
{
  m_forwardDuration = value;
}

qint64 FtPlayer::rewindDuration() const
{
  return m_rewindDuration;
}

void FtPlayer::setRewindDuration(qint64 value)
{
  m_rewindDuration = value;
}

qint64 FtPlayer::playRewindDuration() const
{
  return m_playRewindDuration;
}

void FtPlayer::setPlayRewindDuration(qint64 value)
{
  m_playRewindDuration = value;
}


int FtPlayer::volume() const {
  return m_player.volume();
}

void FtPlayer::setVolume(int in_volume) {
  m_player.setVolume(in_volume);
}

void FtPlayer::decreaseVolume() {
    setVolume(volume() - 1);
}

void FtPlayer::increaseVolume() {
    setVolume(volume() + 1);
}

qint64 FtPlayer::position() const {
  return m_player.position();
}

void FtPlayer::setPosition(qint64 in_position) {
  qDebug() << "setPosition" << in_position << "current position" << position();
  m_player.setPosition(in_position);
}

qint64 FtPlayer::duration() const {
  return m_player.duration();
}

qreal FtPlayer::playbackRate() const
{
  return m_player.playbackRate();
}

void FtPlayer::play() {
  m_player.play();
}

void FtPlayer::pause() {
  qDebug() << "pause, position" << position();
  m_player.pause();
}

void FtPlayer::stop() {
  m_player.stop();
}

void FtPlayer::rewind() {
  rewind(m_rewindDuration);
}

void FtPlayer::rewind(qint64 in_duration) {
  setPosition(std::max((qint64) 0, position() - in_duration));
}

void FtPlayer::forward() {
  forward(m_forwardDuration);
}

void FtPlayer::forward(qint64 in_duration) {
  setPosition(std::min(duration(), position() + in_duration));
}

void FtPlayer::playRewind() {
  qDebug() << "playRewind" << m_playRewindDuration << "position" << position();
  playRewind(m_playRewindDuration);
}

void FtPlayer::playRewind(qint64 in_rewindDuration) {
    rewind(in_rewindDuration);
    m_player.play();
}

void FtPlayer::togglePause() {
  //if (state == QMediaPlayer::PlayingState) {
  if (state() == FtPlayer::PlayingState) {
    pause();
  }
  else {
    playRewind(m_playRewindDuration);
  }
}

void FtPlayer::setPlaybackRate(qreal in_rate)
{
  m_player.setPlaybackRate(in_rate);
}


void FtPlayer::restoreState() {
  if (m_previousState != state()) {
    switch (m_previousState) {
      case FtPlayer::PlayingState:
        m_player.play();
        break;
      case FtPlayer::PausedState:
        m_player.pause();
        break;
      case FtPlayer::StoppedState:
        m_player.stop();
        break;
    }
  }
}

void FtPlayer::saveState()
{
  m_previousState = state();
}

void FtPlayer::setMedia(FtMedia &in_media)
{
  m_loadingState = 1;
  m_currentMedia = in_media;
  m_player.setMedia(in_media.mediaUrl());
}

void FtPlayer::setMedia()
{
  m_loadingState = 0;
  m_currentMedia = FtMedia();
  m_player.setMedia(QMediaContent());
}

FtPlayer::State FtPlayer::state() const {
  return m_state;
}


void FtPlayer::onPlayerMediaStatusChanged(QMediaPlayer::MediaStatus in_status)
{
  qDebug() << "onPlayerMediaStatusChanged" << in_status
           << "status" << m_player.mediaStatus()
           << "state" << m_player.state()
           << "position" << position()
           << "current media pos" << m_currentMedia.lastTime();
  if (m_loadingState == 1 && in_status == QMediaPlayer::LoadedMedia) {
    m_player.play();
  }
}


void FtPlayer::onPlayerStateChanged(QMediaPlayer::State in_state)
{
  qDebug() << "statechanged" << in_state
           << "state" << m_player.state()
           << "status" << m_player.mediaStatus()
           << "position" << position()
           << "current media pos" << m_currentMedia.lastTime();
   if (!m_loadingState) {
    switch (in_state) {
      case QMediaPlayer::StoppedState:
        setState(FtPlayer::StoppedState);
        break;
      case QMediaPlayer::PlayingState:
        setState(FtPlayer::PlayingState);
        break;
      case QMediaPlayer::PausedState:
        setState(FtPlayer::PausedState);
        break;
    }
  }
}

void FtPlayer::onPlayerSeekableChanged(bool in_seekable) {
  qDebug() << "seekablechanged" << in_seekable
           << "position" << m_player.position()
           << "current media pos" <<  m_currentMedia.lastTime()
           << "state" << m_player.state()
           << "status" << m_player.mediaStatus()
           << "loading" << m_loadingState;
  if (in_seekable && m_loadingState == 1) {
    setPosition(m_currentMedia.lastTime());
    m_loadingState = 0;
    pause();
  }
}

void FtPlayer::onPlayerPositionChanged(qint64 in_position)
{
  qDebug() << "poschanged" << in_position
           << "position" << m_player.position()
           << "current media pos" <<  m_currentMedia.lastTime()
           << "state" << m_player.state()
           << "status" << m_player.mediaStatus()
           << "loading" << m_loadingState
           << "seekable" << m_player.isSeekable();

  if (!m_loadingState) {
    emit positionChanged(in_position);
  }
}

void FtPlayer::setState(FtPlayer::State in_state)
{
  m_state = in_state;
  emit stateChanged(m_state);
}
