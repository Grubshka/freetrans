
#include "ftmedialistview.h"
#include "ftmedialistmodel.h"
#include <QStyledItemDelegate>
#include <QApplication>

FtMediaListView::FtMediaListView(QWidget *parent) :
  QListView(parent)
{

}

FtMediaListView::~FtMediaListView()
{

}

/*
void FtMediaListView::addMedia(const FtMedia& in_media)
{
  QListWidgetItem* listItem = new QListWidgetItem(this);
  // listItem->setIcon(QIcon(pixmap));
  // listItem->setData(Qt::UserRole, QVariant(pixmap));
  listItem->setText(in_media.mediaUrl().toString());
  listItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
}
*/


FtMediaListDelegate::FtMediaListDelegate(QObject *parent)
{

}

FtMediaListDelegate::~FtMediaListDelegate()
{

}

void FtMediaListDelegate::paint (QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const {
/*
  //GET TITLE, DESCRIPTION AND ICON
  QIcon ic = QIcon(qvariant_cast<QPixmap>(index.data(Qt::DecorationRole)));
  QString title = index.data(Qt::DisplayRole).toString();
  QString description = index.data(Qt::UserRole + 1).toString();

  int imageSpace = 10;
  if (!ic.isNull()) {
      //ICON
      r = option.rect.adjusted(5, 10, -10, -10);
      ic.paint(painter, r, Qt::AlignVCenter|Qt::AlignLeft);
      imageSpace = 55;
    }

  //TITLE
  r = option.rect.adjusted(imageSpace, 0, -10, -30);
  painter->setFont( QFont( "Lucida Grande", 6, QFont::Normal ) );
  painter->drawText(r.left(), r.top(), r.width(), r.height(), Qt::AlignBottom|Qt::AlignLeft, title, &r);

  //DESCRIPTION
  r = option.rect.adjusted(imageSpace, 30, -10, 0);
  painter->setFont( QFont( "Lucida Grande", 5, QFont::Normal ) );
  painter->drawText(r.left(), r.top(), r.width(), r.height(), Qt::AlignLeft, description, &r);
  */


    //QStyledItemDelegate::paint(painter,option,index);

    painter->save();

    FtMediaListModel::PlayState state = qvariant_cast<FtMediaListModel::PlayState>(index.data(playStateRole));
    QString fileName = qvariant_cast<QString>(index.data(fileNameRole));
    QString url = qvariant_cast<QString>(index.data(urlRole));

    QPen linePen(QColor::fromRgb(211,211,211), 1, Qt::SolidLine); //Color: #C4C4C4
    QPen lineMarkedPen(QColor::fromRgb(0,90,131), 1, Qt::SolidLine); //Color: #005A83
    QPen fontPen(QColor::fromRgb(51,51,51), 1, Qt::SolidLine); //Color: #333
    QPen fontMarkedPen(Qt::white, 1, Qt::SolidLine); //Color: #fff

    QFont font = QApplication::font();
    //font.setPixelSize(font.weight()+);
    if (state) font.setBold(true);
    QFontMetrics fm(font);

    QIcon icon;
    if (state == FtMediaListModel::playing)
        icon = QIcon(":/img/play.png");
    else if (state == FtMediaListModel::paused)
        icon = QIcon(":/img/pause.png");

    QSize iconsize = icon.actualSize(option.decorationSize);
    QRect rowRect = option.rect;
    QRect headerRect = option.rect;
    QRect iconRect = option.rect;

    iconRect.setLeft(iconRect.left()+4);
    iconRect.setRight(iconRect.left()+16);
    iconRect.setTop(iconRect.top()+4);
    iconRect.setBottom(iconRect.top()+16);
    headerRect.setLeft(headerRect.left()+30);
    headerRect.setTop(headerRect.top()+5);
    headerRect.setBottom(headerRect.top()+fm.height());

    if(option.state & QStyle::State_Selected){
        QLinearGradient gradientSelected(rowRect.left(),rowRect.top(),rowRect.left(),rowRect.height()+rowRect.top());
        gradientSelected.setColorAt(0.0, QColor::fromRgb(119,213,247));
        gradientSelected.setColorAt(0.9, QColor::fromRgb(27,134,183));
        gradientSelected.setColorAt(1.0, QColor::fromRgb(0,120,174));
        painter->fillRect(rowRect, gradientSelected);
        painter->setPen(fontMarkedPen);

      }
      else {
        painter->fillRect(rowRect, (index.row() % 2) ? Qt::white : QColor(250,250,250));
        painter->setPen(fontPen);
     }

    //if (state) painter->drawPixmap(QPoint(iconRect.left(),iconRect.top()),icon.pixmap(iconsize.width(),iconsize.height()));
    //if (state) painter->drawPixmap(QPoint(iconRect.left()+iconsize.width()/2+2,iconRect.top()+iconsize.height()/2+3),icon.pixmap(iconsize.width(),iconsize.height()));
    if (state) painter->drawPixmap(iconRect, icon.pixmap(iconsize.width(),iconsize.height()));

    painter->setFont(font);
    painter->drawText(headerRect, fileName);

    /*
    painter->setFont(SubFont);
    painter->drawText(subheaderRect.left(),subheaderRect.top()+17,subText);
    */

    painter->restore();
}

QSize FtMediaListDelegate::sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const{
    QIcon icon = qvariant_cast<QIcon>(index.data(Qt::DecorationRole));
    QSize iconsize = icon.actualSize(option.decorationSize);
    QFont font = QApplication::font();
    QFontMetrics fm(font);

    return(QSize(0, 24));
}

