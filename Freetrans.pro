#-------------------------------------------------
#
# Project created by QtCreator 2016-06-01T00:18:49
#
#-------------------------------------------------

QT       += core gui widgets multimedia gui-private x11extras

TARGET = Freetrans
TEMPLATE = app

SOURCES += \
           main.cpp\
    qxt/qxtglobal.cpp \
    qxt/qxtglobalshortcut.cpp \
    qxt/qxtapplication.cpp \
    ftplayer.cpp \
    ftmedia.cpp \
    ftmedialist.cpp \
    ftmedialistmodel.cpp \
    ftmedialistproviderdir.cpp \
    ftmedialistfile.cpp \
    kislider.cpp \
    ftmediamark.cpp \
    ftwindow.cpp \
    qxt/qxtwindowsystem.cpp \
    ftmedialistview.cpp \
    kidefaultlabel.cpp \
    settingsdialog.cpp \
    kitimeslider.cpp

HEADERS  += \
    qxt/mac/qxtwindowsystem_mac.h \
    qxt/qxtglobal.h \
    qxt/qxtglobalshortcut.h \
    qxt/qxtglobalshortcut_p.h \
    qxt/qxtapplication.h \
    qxt/qxtapplication_p.h \
    ftplayer.h \
    ftmedia.h \
    ftmedialist.h \
    ftmedialistmodel.h \
    ftmedialistprovidervirtual.h \
    ftmedialistproviderdir.h \
    ftmedialistfile.h \
    kislider.h \
    ftmediamark.h \
    ftwindow.h \
    qxt/qxtwindowsystem.h \
    ftmedialistview.h \
    kidefaultlabel.h \
    settingsdialog.h \
    kitimeslider.h

FORMS    += mainwindow.ui \
    settingsdialog.ui

unix:!macx {
  SOURCES += \
    qxt/x11/qxtwindowsystem_x11.cpp \
      qxt/x11/qxtglobalshortcut_x11.cpp
}
macx {
  SOURCES += \
    qxt/mac/qxtwindowsystem_mac.cpp \
    qxt/mac/qxtglobalshortcut_mac.cpp
  HEADERS  += \
    qxt/mac/qxtwindowsystem_mac.h \

}
win32 {
  SOURCES += \
    qxt/win/qxtwindowsystem_win.cpp \
    qxt/win/qxtglobalshortcut_win.cpp
}

INCLUDEPATH += qxt/

QMAKE_CXXFLAGS += -lX11

QMAKE_LFLAGS += -lX11

CONFIG += c++11

#CONFIG += link_pkgconfig
#PKGCONFIG += Qt5GStreamer-1.0


#CONFIG += link_pkgconfig
#PKGCONFIG += x11

DISTFILES += \
    app.qmodel \
    ../../../../Téléchargements/Add File-80.png \
    img/Add File-80.png

RESOURCES += \
    resources.qrc
