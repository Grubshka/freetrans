#include "ftmedialistproviderdir.h"
#include <QDebug>

FtMediaListProviderDir::FtMediaListProviderDir(const QDir & dir)
    : m_dir(dir)
{

}

QVector<FtMediaListStorable*> FtMediaListProviderDir::getAll() const
{
  QStringList files = m_dir.entryList(QDir::Files);
  // XXX TODO (sync filename with list title ?)
}

FtMediaListStorable *FtMediaListProviderDir::get(const QString & in_listName) const
{
    QString listName(in_listName);
    QFile* file = new QFile(m_dir.filePath(listName.append(".json")));
    FtMediaListFile* mediaListFile = new FtMediaListFile(file);
    return mediaListFile;
}
