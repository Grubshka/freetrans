#include "ftmedialistmodel.h"
#include "ftmedialistview.h"
#include <QIcon>
#include <QModelIndexList>

FtMediaListModel::FtMediaListModel(FtMediaList *in_mediaList, QObject *parent) :
  QAbstractListModel(parent),
  m_mediaList(in_mediaList)
{
  connect(m_mediaList, &FtMediaList::mediaInserted,  this, &FtMediaListModel::onMediaList_mediaInserted);
  connect(m_mediaList, &FtMediaList::mediaRemoved,   this, &FtMediaListModel::onMediaList_mediaRemoved);
  connect(m_mediaList, &FtMediaList::currentChanged, this, &FtMediaListModel::onMediaList_currentChanged);
}

QVariant FtMediaListModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid()) return QVariant();

  if (role == FtMediaListDelegate::playStateRole) {
    if (m_currentIndex == index)
      if (m_playing) return qVariantFromValue(PlayState::playing);
      else return qVariantFromValue(PlayState::paused);
    else return qVariantFromValue(PlayState::none);
  }
  else if (role == Qt::ToolTipRole || role == FtMediaListDelegate::urlRole) {
    return m_mediaList->at(index.row()).mediaUrl().toString();
  }
  else if (role == Qt::DisplayRole || role == FtMediaListDelegate::fileNameRole) {
    return m_mediaList->at(index.row()).mediaUrl().fileName();
  }
  return QVariant();
}

// Qt::ItemFlags FtMediaListModel::flags(const QModelIndex &index)

FtMedia& FtMediaListModel::getMediaAt(const QModelIndex &index) const {
  return m_mediaList->at(index.row());
}


bool FtMediaListModel::removeRows(int in_row, int in_count, const QModelIndex &parent) {
  beginRemoveRows(parent, in_row, in_row + in_count - 1);
  m_mediaList->remove(in_row, in_count);
  endRemoveRows();
  return true;
}

/*bool FtMediaListModel::dropMimeData(const QMimeData *data, Qt::DropAction action,
                  int row, int column, const QModelIndex &parent) Q_DECL_OVERRIDE;
QMimeData *FtMediaListModel::mimeData(const QModelIndexList &indexes) const Q_DECL_OVERRIDE;
QStringList FtMediaListModel::mimeTypes() const Q_DECL_OVERRIDE;
*/

int FtMediaListModel::rowCount(const QModelIndex &parent) const {
  return m_mediaList->count();
}

void FtMediaListModel::onMediaList_currentChanged()
{
  if (m_mediaList->hasCurrent()) {
    FtMedia media = *m_mediaList->current();
    QModelIndexList matchList = match(createIndex(0,0), FtMediaListDelegate::urlRole, media.mediaUrl().toString(), 1, Qt::MatchExactly);
    if (matchList.length() > 0) m_currentIndex = QPersistentModelIndex(matchList.at(0));
    else m_currentIndex = QPersistentModelIndex();
  }
  else m_currentIndex = QPersistentModelIndex();
  emit dataChanged(QModelIndex(), QModelIndex());
}

void FtMediaListModel::onMediaList_mediaInserted(int in_mediaIndex)
{
  emit dataChanged(QModelIndex(), QModelIndex());
}

void FtMediaListModel::onMediaList_mediaRemoved(int in_mediaIndex)
{
    emit dataChanged(QModelIndex(), QModelIndex());
}

void FtMediaListModel::onPlayStateChanged(bool playing)
{
    m_playing = playing;
    emit dataChanged(QModelIndex(), QModelIndex());
}



//Qt::DropActions FtMediaListModel::supportedDropActions() const Q_DECL_OVERRIDE;
