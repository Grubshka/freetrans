#ifndef KISLIDER_H
#define KISLIDER_H

#include <QSlider>

class KiSlider: public QSlider
{
public:
    KiSlider(QWidget *parent = Q_NULLPTR);
    KiSlider(Qt::Orientation orientation, QWidget *parent = Q_NULLPTR);
protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
};

#endif // KISLIDER_H
