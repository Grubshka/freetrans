#ifndef KITIMESLIDER_H
#define KITIMESLIDER_H

#include "kislider.h"
#include <QMouseEvent>

class KiTimeSlider : public KiSlider
{
    using KiSlider::KiSlider;
public:
    //KiTimeSlider();
    void mouseMoveEvent(QMouseEvent * event);
};

#endif // KITIMESLIDER_H
