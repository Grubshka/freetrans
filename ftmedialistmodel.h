#ifndef FTMEDIALISTMODEL_H
#define FTMEDIALISTMODEL_H

#include <QAbstractListModel>
#include <QPersistentModelIndex>
#include "ftmedialist.h"

class FtMediaListModel : public QAbstractListModel
{
  Q_OBJECT
public:
  explicit FtMediaListModel(FtMediaList *in_mediaList, QObject *parent = 0);

  enum PlayState {
      none = 0,
      playing = 1,
      paused = 2
  };

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

  // Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
  bool removeRows(int in_row, int in_count, const QModelIndex &parent) Q_DECL_OVERRIDE;

  FtMedia& getMediaAt(const QModelIndex &index) const;
  /*
  bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                    int row, int column, const QModelIndex &parent) Q_DECL_OVERRIDE;
  QMimeData *mimeData(const QModelIndexList &indexes) const Q_DECL_OVERRIDE;
  QStringList mimeTypes() const Q_DECL_OVERRIDE;
  */

  int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
  // Qt::DropActions supportedDropActions() const Q_DECL_OVERRIDE;

public slots:
  void onPlayStateChanged(bool playing);

private slots:
  void onMediaList_currentChanged();
  void onMediaList_mediaInserted(int in_mediaIndex);
  void onMediaList_mediaRemoved(int in_mediaIndex);

private:
  FtMediaList* m_mediaList;
  QPersistentModelIndex m_currentIndex;
  bool m_playing;
};

Q_DECLARE_METATYPE(FtMediaListModel::PlayState)

#endif // FTMEDIALISTMODEL_H
