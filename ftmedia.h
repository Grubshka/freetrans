#ifndef FTMEDIA_H
#define FTMEDIA_H

#include <QUrl>
#include <QJsonObject>
#include <QString>
#include <memory>
#include <map>
#include "ftmediamark.h"


/**
 * @brief The FtMedia class to manage a media and its bookmarks
 */
class FtMedia
{
public:
  FtMedia();
  FtMedia(const QUrl &in_mediaUrl);
  FtMedia(const QUrl &in_mediaUrl, const QString &in_title);

  QUrl mediaUrl() const;
  void setMediaUrl(const QUrl &in_mediaUrl);

  void read(const QJsonObject &in_json);
  void write(QJsonObject &out_json) const;

  QString title() const;
  void setTitle(const QString &in_title);

  qint64 lastTime() const;
  void setLastTime(const qint64 &in_lastTime);

  bool equals(const QUrl& in_url) const;
  bool equals(const FtMedia& in_media) const;

  void addMarker(const FtMediaMark& in_marker);

private:
  QUrl m_mediaUrl;
  QString m_title;
  qint64 m_lastTime;
  std::map<qint64, FtMediaMark> m_markers;

};



#endif // FTMEDIA_H
