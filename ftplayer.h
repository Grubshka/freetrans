#ifndef FTPLAYER_H
#define FTPLAYER_H

#include <QObject>
#include <QMediaPlayer>
#include <ftmedia.h>

class FtPlayer: public QObject {
  Q_OBJECT

public:
  enum State
  {
    StoppedState,
    PlayingState,
    PausedState
    //LoadingState
  };


  FtPlayer();
  FtPlayer(qint64 in_playRewindDuration,
           qint64 in_rewindDuration,
           qint64 in_forwardDuration);

  qint64 playRewindDuration() const;
  void setPlayRewindDuration(qint64 value);

  qint64 rewindDuration() const;
  void setRewindDuration(qint64 value);

  qint64 forwardDuration() const;
  void setForwardDuration(qint64 value);

  void setMedia();
  void setMedia(FtMedia& in_media);

  State state() const;
  int volume() const;
  qint64 position() const;
  qint64 duration() const;
  qreal playbackRate() const;

public slots:
  void setVolume(int in_volume);
  void decreaseVolume();
  void increaseVolume();

  void setPosition(qint64 in_position);
  void play();
  void pause();
  void stop();

  void rewind();
  void rewind(qint64 in_duration);
  void forward();
  void forward(qint64 in_duration);

  //void play();
  void playRewind();
  void playRewind(qint64 in_rewindDuration);
  void togglePause();

  void setPlaybackRate(qreal in_rate);

  void onPlayerMediaStatusChanged(QMediaPlayer::MediaStatus in_status);
  void onPlayerStateChanged(QMediaPlayer::State in_state);
  void onPlayerPositionChanged(qint64 in_position);
  void onPlayerSeekableChanged(bool in_seekable);

  //void setState(QMediaPlayer::State state);
  void saveState();
  void restoreState();


signals:
  void positionChanged(qint64 position);
  void stateChanged(FtPlayer::State state);
  void durationChanged(qint64 duration);
  void volumeChanged(int volume);
  void playbackRateChanged(qreal rate);
  // void mediaStatusChanged(FtPlayer::MediaStatus status);

protected:
  qint64 m_playRewindDuration;
  qint64 m_rewindDuration;
  qint64 m_forwardDuration;

  int m_loadingState;
  State m_previousState;
  State m_state;

  FtMedia m_currentMedia;

  QMediaPlayer m_player;


  void setState(FtPlayer::State in_state);
};

#endif // FTPLAYER_H
