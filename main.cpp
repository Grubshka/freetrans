#include "ftwindow.h"
#include <qxtapplication.h>

int main(int argc, char *argv[])
{
    QxtApplication a(argc, argv);
    FtWindow w;
    w.show();

    return a.exec();
}
