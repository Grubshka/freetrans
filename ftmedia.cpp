#include <QJsonArray>
#include "ftmedia.h"

FtMedia::FtMedia() :
  m_lastTime(0)
{

}

FtMedia::FtMedia(const QUrl &in_mediaUrl) :
  m_mediaUrl(in_mediaUrl),
  m_lastTime(0)
{

}

FtMedia::FtMedia(const QUrl &in_mediaUrl, const QString &in_title) :
  m_mediaUrl(in_mediaUrl),
  m_title(in_title),
  m_lastTime(0)
{

}

QUrl FtMedia::mediaUrl() const
{
  return m_mediaUrl;
}

void FtMedia::setMediaUrl(const QUrl &in_mediaUrl)
{
  m_mediaUrl = in_mediaUrl;
}

void FtMedia::read(const QJsonObject &in_json)
{
  m_mediaUrl = QUrl(in_json["url"].toString());
  m_title = in_json["title"].toString();
  m_lastTime = in_json["lasttime"].toDouble();

  QJsonArray markerArray = in_json["markers"].toArray();
  for (int markerIndex = 0; markerIndex < markerArray.size(); ++markerIndex) {
      QJsonObject markerObject = markerArray[markerIndex].toObject();
      FtMediaMark marker;
      marker.read(markerObject);
      m_markers[marker.time()] = marker;
  }
}

void FtMedia::write(QJsonObject &out_json) const
{
  out_json["url"] = m_mediaUrl.toString();
  out_json["title"] = m_title;
  out_json["lasttime"] = m_lastTime;

  QJsonArray markerArray;
  for (const auto& kv : m_markers) {
      QJsonObject markerObject;
      kv.second.write(markerObject);
      markerArray.append(markerObject);
  }
  out_json["markers"] = markerArray;
}

QString FtMedia::title() const
{
  return m_title;
}

void FtMedia::setTitle(const QString &in_title)
{
  m_title = in_title;
}

qint64 FtMedia::lastTime() const
{
  return m_lastTime;
}

void FtMedia::setLastTime(const qint64 &in_lastTime)
{
  m_lastTime = in_lastTime;
}

bool FtMedia::equals(const QUrl& in_url) const {
  return mediaUrl() == in_url;
}

bool FtMedia::equals(const FtMedia& in_media) const {
  return mediaUrl() == in_media.mediaUrl();
}

void FtMedia::addMarker(const FtMediaMark &in_marker)
{
  m_markers[in_marker.time()] = in_marker;
}


