#ifndef FTMEDIALISTPROVIDERDIR_H
#define FTMEDIALISTPROVIDERDIR_H

#include <ftmedialistprovidervirtual.h>
#include <ftmedialistfile.h>
#include <QVector>
#include <QDir>

class FtMediaListProviderDir : public FtMediaListProviderVirtual
{
public:
    FtMediaListProviderDir(const QDir & dir);

    QVector<FtMediaListStorable*> getAll() const;
    FtMediaListStorable* get(const QString & in_listName) const;

protected:
    QDir m_dir;
};

#endif // FTMEDIALISTPROVIDERDIR_H
