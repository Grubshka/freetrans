#ifndef KEYGRABBER_H
#define KEYGRABBER_H

#include <string>
#include <vector>
#include <set>

using namespace std;

/**
 * @brief The KeyObserver interface
 * XXX should better make a QShortcut extend, like QxtGlobalShortcut.
 */
class KeyObserver
{
public:
    /**
     * @brief keyPressed listener
     * @param key The key string
     */
    virtual void keyPressed(const string key) = 0;
};

/**
 * @brief The KeyGrabber class is a system-wide shortcut listener.
 */
class KeyGrabber
{
public:
    KeyGrabber();
    virtual ~KeyGrabber();

    /**
     * @brief Register multiple keys to be listened
     * @param keys A vector of key strings
     */
    void registerKeys(const vector<string>& keys);

    /**
     * @brief Register a key to be listened
     * @param key A key string
     */
    virtual void registerKey(const string key);

    /**
     * @brief Deregister a key
     * @param key A key string
     */
    virtual void deregisterKey(const string key);

    /**
     * @brief Tell if it's currently grabbing keys
     * @return
     */
    bool isGrabbing();

    /**
     * @brief Start listening to registered keys.
     */
    virtual void start();

    /**
     * @brief Stop listening to registered keys.
     */
    virtual void stop();

    /**
     * @brief Restart listening to registered keys.
     */
    void restart();

    /**
     * @brief Listen to any shortcut
     */
    virtual void onceAnyKey() = 0;

    /**
     * @brief Add an observer, that will be notified for all registered keys.
     * @param obs
     */
    void addObserver(KeyObserver* obs);

    /**
     * @brief Remove an Observer
     * @param obs
     */
    void removeObserver(KeyObserver* obs);

protected:
    bool grabbing;
    vector<string> keys;

    set<KeyObserver*> observers;

    void notify(const string);

    virtual void _registerKey(const string) = 0;
    virtual void _deregisterKey(const string) = 0;

};

#endif // KEYGRABBER_H
