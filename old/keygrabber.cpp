#include <QDebug>
#include "keygrabber.h"
#include <iostream>

KeyGrabber::KeyGrabber() :
    grabbing(false)
{
}

KeyGrabber::~KeyGrabber() {
    stop();
}

void KeyGrabber::registerKeys(const vector<string> &keys)
{
  for (uint k = 0; k < keys.size(); ++k) {
      _registerKey(keys[k]);
  }
}

void KeyGrabber::registerKey(const string key) {
    _registerKey(key);
    if (isGrabbing()) {
        restart();
    }
}

void KeyGrabber::deregisterKey(const string key) {
    _deregisterKey(key);
    if (isGrabbing()) {
        restart();
    }
}

bool KeyGrabber::isGrabbing() {
    return grabbing;
}

void KeyGrabber::start()
{
    if (grabbing) stop();
    grabbing = true;
}

void KeyGrabber::stop()
{
    if (!grabbing) return;
    grabbing = false;
}

void KeyGrabber::restart() {
    stop();
    start();
}

void KeyGrabber::notify(const string key) {
    for( set<KeyObserver*>::iterator i = observers.begin(); i != observers.end(); ++i) {
          (*i)->keyPressed(key);
    }
}

void KeyGrabber::addObserver(KeyObserver* obs) {
    observers.insert(obs);
}

void KeyGrabber::removeObserver(KeyObserver* obs) {
    observers.erase(obs);
}
