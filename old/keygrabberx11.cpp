#include <QDebug>
#include "keygrabberx11.h"
#include <X11/Xlib.h>
#include <iostream>
#include <thread>

KeyGrabberX11::KeyGrabberX11() :
    KeyGrabber()
{
    XInitThreads();
    dpy = XOpenDisplay(0);
    if (!dpy) {
        // qCritical() << "No display found";
        std::cerr << "No display found";
        return;
    }

    root = &DefaultRootWindow(dpy);

}

KeyGrabberX11::~KeyGrabberX11()
{
}

void KeyGrabberX11::_registerKey(const string key) {
    keys.push_back(key);
    keycodes.push_back(XKeysymToKeycode(dpy, XStringToKeysym(key.c_str())));
}

void KeyGrabberX11::_deregisterKey(const string key) {
}

void KeyGrabberX11::start()
{
    if (!grabbing) {
        grabbing = true;

        for (uint k = 0; k < keycodes.size(); ++k) {
          // qDebug() << " start grabbing " << keycodes[k];
          XGrabKey(dpy, keycodes[k], AnyModifier, *root, True, GrabModeAsync, GrabModeAsync);
        }

        // without a thread, it's freezing the interface
        //_start();

        grabThread = std::thread(&KeyGrabberX11::_start, this);
        grabThread.detach();

    }
}

void KeyGrabberX11::_start() {
    XEvent e;

    for(;grabbing;) {
        // qDebug() << " in thread " << grabbing;
        XNextEvent(dpy, &e);
        // qDebug() << " in thread 2 " << grabbing;
        if (e.type == KeyPress) {
            XKeyEvent* xke = (XKeyEvent *)&e;
            // qDebug() << "key " << xke->state << " " << xke->keycode;
            notify(XKeysymToString(XKeycodeToKeysym(dpy, xke->keycode, 0)));
        }
    }
}

// XXX DOES NOT WORK! Key is not release and is unusable until process ends
void KeyGrabberX11::stop()
{
    qDebug() << " stop " << grabbing;
    if (grabbing) {
        for (uint k = 0; k < keycodes.size(); ++k) {
            // qDebug() << " stop grabbing " << keycodes[k];
            XUngrabKey(dpy, keycodes[k], AnyModifier, *root);
        }
        grabbing = false;
    }
}

void KeyGrabberX11::onceAnyKey() {

}
