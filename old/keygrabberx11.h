#ifndef KEYGRABBERX11_H
#define KEYGRABBERX11_H

#include "keygrabber.h"
#include <X11/Xlib.h>
#include <thread>

class KeyGrabberX11: public KeyGrabber
{
public:
    KeyGrabberX11();
    ~KeyGrabberX11();

    void start();
    void stop();

    void onceAnyKey();

protected:
    void _registerKey(const string);
    void _deregisterKey(const string);

private:
    Window *root;
    Display *dpy;
    vector<int> keycodes;
    thread grabThread;

    void _start();
};

#endif // KEYGRABBERX11_H
