#include "ftmedialist.h"
#include <QJsonArray>

FtMediaList::FtMediaList()
{

}

std::vector<FtMedia>::iterator FtMediaList::find(const FtMedia& in_media) const
{

  return std::find_if(m_medias.begin(), m_medias.end(), [&in_media](const FtMedia& media) {
    return media.equals(in_media);
  });
/*
  std::vector<FtMedia>::iterator it;
  std::vector<FtMedia>::iterator found;
  for (it = m_medias.begin(), found = m_medias.end(); found == m_medias.end() && it < m_medias.end(); ++it) {
    if ((*it).equals(in_media)) {
      found = it;
    }
  }
  return found;
  */
}

std::vector<FtMedia>::iterator FtMediaList::find(const QUrl& in_url) const
{
  return std::find_if(m_medias.begin(), m_medias.end(), [&in_url](const FtMedia& media) {
    return media.equals(in_url);
  });
/*
  std::vector<FtMedia>::iterator it;
  std::vector<FtMedia>::iterator found;
  for (it = m_medias.begin(), found = m_medias.end(); found == m_medias.end() && it < m_medias.end(); ++it) {
    if ((*it).equals(in_url)) {
      found = it;
    }
  }
  return found;
*/
}

bool FtMediaList::contains(const FtMedia &in_media) const
{
  return m_medias.end() != find(in_media);
}

bool FtMediaList::contains(const QUrl &in_url) const
{
  return m_medias.end() != find(in_url);
}

FtMedia& FtMediaList::at(const int &in_index)
{
  return m_medias.at(in_index);
}

int FtMediaList::count()
{
  return m_medias.size();
}

void FtMediaList::append(const FtMedia &in_media)
{
  if (!contains(in_media)) {
    m_medias.push_back(in_media);
    emit mediaInserted(m_medias.size()-1);
  }
}

void FtMediaList::remove(const int &in_mediaIndex)
{
   if (m_current == m_medias.begin()+in_mediaIndex) {
       setCurrent();
   }
   m_medias.erase(m_medias.begin()+in_mediaIndex);
}

void FtMediaList::remove(const int &in_mediaIndex, const int &in_count)
{
    if (m_current >= m_medias.begin()+in_mediaIndex && m_current < m_medias.begin()+in_mediaIndex+in_count) {
        setCurrent();
    }
   m_medias.erase(m_medias.begin()+in_mediaIndex, m_medias.begin()+in_mediaIndex+in_count);
}

void FtMediaList::insert(const FtMedia &in_media, const int& in_mediaIndex)
{
  if (!contains(in_media)) {
    m_medias.insert(m_medias.begin()+in_mediaIndex, in_media);
    emit mediaInserted(m_medias.size()-1);
  }
}

void FtMediaList::remove(const FtMedia &in_media)
{
    m_medias.erase(find(in_media));
}

bool FtMediaList::hasCurrent() const
{
    return m_current != m_medias.end();
}

std::vector<FtMedia>::iterator FtMediaList::current() const
{
  return m_current;
}

void FtMediaList::setCurrent(const FtMedia &in_media)
{
  m_current = find(in_media);
  emit currentChanged();
}

void FtMediaList::setCurrent(const int &in_mediaIndex)
{
  m_current = m_medias.begin() + in_mediaIndex;
  emit currentChanged();
}

void FtMediaList::setCurrent(const std::vector<FtMedia>::iterator &in_mediaIterator)
{
  m_current = in_mediaIterator;
  emit currentChanged();
}

void FtMediaList::setCurrent()
{
    m_current = m_medias.end();
    emit currentChanged();
}

QString FtMediaList::title() const
{
  return m_title;
}

void FtMediaList::setTitle(const QString &in_title)
{
  m_title = in_title;
}
