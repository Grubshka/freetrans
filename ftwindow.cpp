#include "ftwindow.h"
#include "ui_mainwindow.h"
#include "settingsdialog.h"

#include <QSettings>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QJsonDocument>
#include <QByteArray>
#include <QTime>
#include <QStandardPaths>
#include <math.h>

FtWindow::FtWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::FtWindow),
    m_player(),
    m_localProvider(getDefaultProviderDir()),
    m_currentMediaList(m_localProvider.get("default"))
{
    QCoreApplication::setOrganizationName("Kabukis");
    QCoreApplication::setOrganizationDomain("kabukis.fr");
    QCoreApplication::setApplicationName("FreeTrans");
    QCoreApplication::setApplicationVersion("1.0a");
    setWindowIcon(QIcon(":/img/kabukis_icon.png"));

    m_ui->setupUi(this);
    m_ui->currentMediaLabel->setDefaultText(tr("No media loaded"));
    m_ui->currentTimeLabel->setDefaultText("--:--:--");
    m_ui->maxTimeLabel->setDefaultText("--:--:--");

    setShortcuts();

    // player=>ui connections
    connect(&m_player, &FtPlayer::volumeChanged,       this, &FtWindow::onPlayerVolumeChanged);
    connect(&m_player, &FtPlayer::playbackRateChanged, this, &FtWindow::onPlayerPlaybackRateChanged);
    connect(&m_player, &FtPlayer::positionChanged,     this, &FtWindow::onPlayerPositionChanged);
    connect(&m_player, &FtPlayer::durationChanged,     this, &FtWindow::onPlayerDurationChanged);
    connect(&m_player, &FtPlayer::stateChanged,        this, &FtWindow::onPlayerStateChanged);
    /* connect(&m_player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
            this, SLOT(onPlayerMediaStatusChanged(QMediaPlayer::MediaStatus)));
    */

    //m_ui->rewindButton->setDefaultAction(m_ui->actionRewind);
    m_ui->shortRewindButton->setDefaultAction(m_ui->actionShortRewind);
    m_ui->playButton->setDefaultAction(m_ui->actionPlayPause);
    m_ui->shortForwardButton->setDefaultAction(m_ui->actionShortForward);
    //m_ui->forwardButton->setDefaultAction(m_ui->actionForward);

   connect(m_currentMediaList, &FtMediaList::currentChanged, this, &FtWindow::onMediaList_currentChanged);

    m_mediaListModel = new FtMediaListModel(m_currentMediaList, m_ui->mediaList);
    m_ui->mediaList->setModel(m_mediaListModel);
    m_mediaListSelectionModel = new QItemSelectionModel(m_mediaListModel, m_ui->mediaList);
    m_ui->mediaList->setSelectionModel(m_mediaListSelectionModel);
    m_ui->mediaList->setItemDelegate(new FtMediaListDelegate(m_ui->mediaList));
    connect(m_ui->mediaList->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FtWindow::onMediaListSelectionModel_selectionChanged);
    connect(m_ui->mediaList->model(), &FtMediaListModel::dataChanged, this, &FtWindow::onMediaListModel_dataChanged);

    m_ui->addButton->setDefaultAction(m_ui->actionAdd);
    m_ui->selectAllButton->setDefaultAction(m_ui->actionSelectAll);
    m_ui->removeButton->setDefaultAction(m_ui->actionRemove);
    updateMediaListUI();
    updatePlayerUI();

    m_currentMediaList->read();
}

FtWindow::~FtWindow()
{
    delete m_ui;
}

void FtWindow::setShortcuts() {
  if (m_shortcuts.length()) unsetShortcuts();

  QSettings settings;
  settings.beginGroup("player");

  QString playPauseKeyStr = settings.value("playPause/shortcut", "F7").toString();
  if (playPauseKeyStr.length()) {
      QxtGlobalShortcut* playPauseShortcut = new QxtGlobalShortcut(this);
      QKeySequence playPauseKey(playPauseKeyStr);
      if (!playPauseShortcut->setShortcut(playPauseKey))
        qDebug() << "Can't set shortcut" << playPauseKey.toString(QKeySequence::NativeText) << " to actionPlayPause";
      else {
          connect(playPauseShortcut, &QxtGlobalShortcut::activated, m_ui->actionPlayPause, &QAction::trigger);
          m_shortcuts.append(playPauseShortcut);
      }
  }

  QString rewindKeyStr = settings.value("rewind/shortcut", "").toString();
  if (rewindKeyStr.length()) {
      QxtGlobalShortcut* rewindShortcut = new QxtGlobalShortcut(this);
      QKeySequence rewindKey(rewindKeyStr);
      if (!rewindShortcut->setShortcut(rewindKey))
        qDebug() << "Can't set shortcut" << rewindKey.toString(QKeySequence::NativeText) << " to actionRewind";
      else {
          connect(rewindShortcut, &QxtGlobalShortcut::activated, m_ui->actionRewind, &QAction::trigger);
          m_shortcuts.append(rewindShortcut);
      }
  }

  QString shortRewindKeyStr = settings.value("shortRewind/shortcut", "F6").toString();
  if (shortRewindKeyStr.length()) {
      QxtGlobalShortcut* shortRewindShortcut = new QxtGlobalShortcut(this);
      QKeySequence shortRewindKey(shortRewindKeyStr);
      if (!shortRewindShortcut->setShortcut(shortRewindKey))
        qDebug() << "Can't set shortcut" << shortRewindKey.toString(QKeySequence::NativeText) << " to actionShortRewind";
      else {
          connect(shortRewindShortcut, &QxtGlobalShortcut::activated, m_ui->actionShortRewind, &QAction::trigger);
          m_shortcuts.append(shortRewindShortcut);
      }
  }

  QString forwardKeyStr = settings.value("forward/shortcut", "").toString();
  if (forwardKeyStr.length()) {
      QxtGlobalShortcut* forwardShortcut = new QxtGlobalShortcut(this);
      QKeySequence forwardKey(forwardKeyStr);
      if (!forwardShortcut->setShortcut(forwardKey))
        qDebug() << "Can't set shortcut" << forwardKey.toString(QKeySequence::NativeText) << " to actionforward";
      else {
          connect(forwardShortcut, &QxtGlobalShortcut::activated, m_ui->actionForward, &QAction::trigger);
          m_shortcuts.append(forwardShortcut);
      }
  }

  QString shortForwardKeyStr = settings.value("shortForward/shortcut", "F8").toString();
  if (shortForwardKeyStr.length()) {
      QxtGlobalShortcut* shortForwardShortcut = new QxtGlobalShortcut(this);
      QKeySequence shortForwardKey(shortForwardKeyStr);
      if (!shortForwardShortcut->setShortcut(shortForwardKey))
        qDebug() << "Can't set shortcut" << shortForwardKey.toString(QKeySequence::NativeText) << " to actionShortforward";
      else {
          connect(shortForwardShortcut, &QxtGlobalShortcut::activated, m_ui->actionShortForward, &QAction::trigger);
          m_shortcuts.append(shortForwardShortcut);
      }
  }

  QString volumeUpKeyStr = settings.value("volumeUp/shortcut", "F11").toString();
  if (volumeUpKeyStr.length()) {
      QxtGlobalShortcut* volumeUpShortcut = new QxtGlobalShortcut(this);
      QKeySequence volumeUpKey(volumeUpKeyStr);
      if (!volumeUpShortcut->setShortcut(volumeUpKey))
        qDebug() << "Can't set shortcut" << volumeUpKey.toString(QKeySequence::NativeText) << " to actionVolumeUp";
      else {
          connect(volumeUpShortcut, &QxtGlobalShortcut::activated, m_ui->actionVolumeUp, &QAction::trigger);
          m_shortcuts.append(volumeUpShortcut);
      }
  }

  QString volumeDownKeyStr = settings.value("volumeDown/shortcut", "F10").toString();
  if (volumeDownKeyStr.length()) {
    QxtGlobalShortcut* volumeDownShortcut = new QxtGlobalShortcut(this);
    QKeySequence volumeDownKey(volumeDownKeyStr);
    if (!volumeDownShortcut->setShortcut(volumeDownKey))
      qDebug() << "Can't set shortcut" << volumeDownKey.toString(QKeySequence::NativeText) << " to actionVolumeDown";
    else {
        connect(volumeDownShortcut, &QxtGlobalShortcut::activated, m_ui->actionVolumeDown, &QAction::trigger);
        m_shortcuts.append(volumeDownShortcut);
    }
  }
}

void FtWindow::unsetShortcuts()
{
    for (QVector<QxtGlobalShortcut*>::iterator it = m_shortcuts.begin(); it != m_shortcuts.end(); ) {
      delete *it;
      it = m_shortcuts.erase(it);
    }
}

void FtWindow::updateMediaListUI()
{
    if (m_mediaListSelectionModel->selectedIndexes().size() > 0) {
      m_ui->actionRemove->setEnabled(true);
    }
    else {
      m_ui->actionRemove->setEnabled(false);
    }

    if (m_mediaListSelectionModel->selectedIndexes().size() < m_mediaListModel->rowCount()) {
        m_ui->actionSelectAll->setEnabled(true);
    }
    else {
        m_ui->actionSelectAll->setEnabled(false);
    }
}

void FtWindow::updatePlayerUI()
{
    bool enabled = m_currentMediaList->hasCurrent();
    m_ui->timeSlider->setEnabled(enabled);
    m_ui->actionRewind->setEnabled(enabled);
    m_ui->actionShortRewind->setEnabled(enabled);
    m_ui->actionPlayPause->setEnabled(enabled);
    m_ui->actionShortForward->setEnabled(enabled);
    m_ui->actionForward->setEnabled(enabled);
    if (!enabled) {
        m_ui->currentMediaLabel->resetText();
        m_ui->currentTimeLabel->resetText();
        m_ui->maxTimeLabel->resetText();
    }
}

void FtWindow::closeEvent(QCloseEvent* event)
{
  m_currentMediaList->write();
  m_player.stop();
  event->accept();
}

void FtWindow::about()
{
    QString qstr = "<b>" + QCoreApplication::applicationName() + " " + QCoreApplication::applicationVersion() + "</b><br /><br />"
            "Free and open-source media player for transcriptions created by "
            "<a href='http://" + QCoreApplication::organizationDomain() + "'>" + QCoreApplication::organizationName() + "</a>. <br /><br />"
            "Please contact the author, <a href='mailto:pierre@kabukis.fr'>Pierre Réveillon</a> "
            "for any question, remark, or contribution.<br />"
            "All icons are from <a href='https://icons8.com/'>Icon8</a>, thanks a lot for their work !";
    QMessageBox::about(this, tr("About Application"), tr(qstr.toStdString().c_str()));
}

QDir FtWindow::getDefaultProviderDir() const {
    QDir dataDir = QDir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    if (!dataDir.exists()) {
      dataDir.mkpath(".");
    }
    QDir listDir = QDir(dataDir.filePath("lists"));
    if (!listDir.exists()) {
      listDir.mkpath(".");
    }

    return listDir;
}


void FtWindow::on_actionQuit_triggered()
{
  m_currentMediaList->write();
  m_player.stop();
  qApp->quit();
}

void FtWindow::on_actionAdd_triggered()
{
    QFileDialog dialog(this);
    dialog.setOption(QFileDialog::DontUseNativeDialog, true);
    QString fileName = dialog.getOpenFileName(this, tr("Open File"), QString(),
            tr("Sound Files (*.mp3 *.wav *.ogg *.m4a)"));

    if (!fileName.isEmpty()) {
        QUrl url = QUrl::fromLocalFile(fileName);
        if (!m_currentMediaList->contains(url)) {
          m_currentMediaList->append(url);
        }
    }
}

void FtWindow::on_actionSettings_triggered()
{
    SettingsDialog* s = new SettingsDialog(this);
    unsetShortcuts();
    s->exec();
    setShortcuts();
}


void FtWindow::on_actionRemove_triggered()
{
  QModelIndexList selected = m_mediaListSelectionModel->selectedIndexes();
  for (int i = selected.size() - 1; i >=0; --i) {
    m_mediaListSelectionModel->select(selected[i], QItemSelectionModel::Deselect);
    m_mediaListModel->removeRows(selected[i].row(), 1, selected[i]);
  }
}

void FtWindow::onPlayerDurationChanged(qint64 in_duration)
{
    m_ui->timeSlider->setMaximum(in_duration);
    //qint64 minutes = floor(in_duration/60000);
    //qint64 seconds = round((in_duration%60000)/1000);
    //m_ui->maxTimeLabel->setText(QString("%1:%2").arg(minutes).arg(seconds, 2, 10, QChar('0')));
    QTime time(0,0,0,0);
    time = time.addMSecs(in_duration);
    m_ui->maxTimeLabel->setText(time.toString());
}

void FtWindow::onPlayerVolumeChanged(int in_volume)
{
  m_ui->volumeSlider->setSliderPosition(in_volume);
}

void FtWindow::onPlayerPositionChanged(qint64 in_position)
{
    qDebug() << "winposchanged" << in_position
             << "playerstate" << m_player.state()
             << "position" << m_player.position();
    qint64 roundedPosition = round(in_position/1000)*1000;
    m_ui->timeSlider->setSliderPosition(in_position);

    //qint64 minutes = floor(in_position/60000);
    //qint64 seconds = round((in_position%60000)/1000);
    //m_ui->currentTimeLabel->setText(QString("%1:%2").arg(minutes).arg(seconds, 2, 10, QChar('0')));
    QTime time(0,0,0,0);
    time = time.addMSecs(in_position);
    m_ui->currentTimeLabel->setText(time.toString());

}

void FtWindow::onPlayerStateChanged(FtPlayer::State in_state)
{
  qDebug() << "winstatechanged" << in_state
           << "position" << m_player.position()
           << "current media pos" <<  m_currentMediaList->current()->lastTime();
  m_ui->actionPlayPause->setChecked(in_state == FtPlayer::PlayingState);
  m_mediaListModel->onPlayStateChanged(in_state == FtPlayer::PlayingState);
  if (in_state != FtPlayer::StoppedState && !m_ui->timeSlider->isEnabled()) {
    m_ui->timeSlider->setEnabled(true);
    m_ui->actionPlayPause->setEnabled(true);
  }
  if (in_state == FtPlayer::PausedState) {
    m_currentMediaList->current()->setLastTime(m_player.position());
  }
}

void FtWindow::onPlayerPlaybackRateChanged(qreal in_rate)
{
  qDebug() << "onPlayerPlaybackRateChanged" << in_rate
           << "rate" << m_player.playbackRate()
           << "position" << m_ui->playbackRateSlider->sliderPosition();
  //m_ui->playbackRateSlider->setSliderPosition(in_rate * 100);
}

/*
void MainWindow::onPlayerMediaStatusChanged(QMediaPlayer::MediaStatus status)
{
  qDebug() << "statuschanged" << status
           << "position" << m_player.position()
           << "current media pos" <<  m_currentMediaList->current()->lastTime();
  if (status == QMediaPlayer::LoadedMedia) {
    m_player.pause();
  }
  else if (status == QMediaPlayer::BufferedMedia) {
    m_player.setPosition(m_currentMediaList->current()->lastTime());
  }
}
*/

void FtWindow::onMediaList_currentChanged()
{
  if (m_currentMediaList->hasCurrent()) {
    FtMedia media = *m_currentMediaList->current();
    m_player.setMedia(media);
    m_ui->currentMediaLabel->setText(media.mediaUrl().fileName());
  }
  else {
    m_player.setMedia();
  }
  updatePlayerUI();
}

void FtWindow::on_timeSlider_sliderPressed()
{
  m_player.saveState();
  if (m_player.state() == FtPlayer::PlayingState) {
    m_player.pause();
  }
}

void FtWindow::on_timeSlider_sliderReleased()
{
  m_player.setPosition(m_ui->timeSlider->sliderPosition());
  m_player.restoreState();
}

void FtWindow::on_timeSlider_sliderMoved(int in_position)
{
    qint64 minutes = floor(in_position/60000);
    qint64 seconds = round((in_position%60000)/1000);
    m_ui->currentTimeLabel->setText(QString("%1:%2").arg(minutes).arg(seconds, 2, 10, QChar('0')));
}

void FtWindow::on_volumeSlider_sliderMoved(int in_position)
{
    m_player.setVolume(in_position);
}

void FtWindow::on_mediaList_doubleClicked(const QModelIndex &index)
{
  m_currentMediaList->setCurrent(m_ui->mediaList->currentIndex().row());
  // m_currentMediaList->setCurrent(in_item->data(Qt::UserRole));
  // m_player.setMedia(in_item->data(Qt::UserRole));
  // m_player.play();
}

void FtWindow::onMediaListSelectionModel_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    updateMediaListUI();
}

void FtWindow::onMediaListModel_dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
    updateMediaListUI();
}

void FtWindow::on_playbackRateSlider_sliderMoved(int position)
{
    qDebug() << "on_playbackRateSlider_sliderMoved" << position
             << "rate" << m_player.playbackRate();

    m_player.setPlaybackRate(position/10);
}

void FtWindow::on_actionPlayPause_triggered(bool in_checked)
{
    if (in_checked) {
      if (m_player.state() != FtPlayer::PlayingState) {
          m_player.playRewind();
      }
    }
    else {
        m_player.pause();
    }
}

void FtWindow::on_actionShortRewind_triggered()
{
    m_player.rewind();
}

void FtWindow::on_actionRewind_triggered()
{
    m_player.rewind();
}

void FtWindow::on_actionShortForward_triggered()
{
    m_player.forward();
}

void FtWindow::on_actionForward_triggered()
{
    m_player.forward();
}

void FtWindow::on_actionVolumeDown_triggered()
{
    m_player.decreaseVolume();
}

void FtWindow::on_actionVolumeUp_triggered()
{
    m_player.increaseVolume();
}

void FtWindow::on_actionAbout_triggered()
{
    about();
}

void FtWindow::on_actionSelectAll_triggered()
{
    QModelIndex parent = QModelIndex();
    QModelIndex topLeft = m_mediaListModel->index(0, 0, parent);
    QModelIndex bottomRight = m_mediaListModel->index(m_mediaListModel->rowCount(parent)-1, 0, parent);
    QItemSelection selection(topLeft, bottomRight);
    m_mediaListSelectionModel->select(selection, QItemSelectionModel::Select);
}
